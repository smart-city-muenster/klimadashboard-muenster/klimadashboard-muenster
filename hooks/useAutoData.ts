// @ts-ignore
import RawAutoData from '@/assets/data/ms_fahrzeuge.csv'

import { useEffect, useState } from 'react'

type InputDataType = {
  ZEIT: string
  RAUM: string
  MERKMAL: string
  WERT: number
}

type AutoDataType = {
  year: number | string
  BenzinAuto: {
    name: string
    value: number
  }
  DieselAuto: {
    name: string
    value: number
  }
  ElektroAuto: {
    name: string
    value: number
  }
  HybridAuto: {
    name: string
    value: number
  }
  SonstigesAuto: {
    name: string
    value: number
  }
  total: number
}

export function dataPrepation(): AutoDataType[] {
  const raumFilter = RawAutoData.filter((d: InputDataType) => {
    return (
      d.RAUM.includes('Gesamtstadt') &&
      (d.MERKMAL.includes('Benzin') ||
        d.MERKMAL.includes('Diesel') ||
        d.MERKMAL.includes('Elektro') ||
        d.MERKMAL.includes('Hybrid') ||
        d.MERKMAL.includes('Sonstige'))
    )
  })
  const years = Array.from<number>(
    new Set(
      raumFilter.map((d: InputDataType) => {
        const year = new Date(
          d.ZEIT.split('.').reverse().join('-'),
        ).getFullYear()
        return year
      }),
    ),
  )

  const outputData: AutoDataType[] = years.map(year => {
    return {
      year: year,
      BenzinAuto: {
        name: 'Benzin',
        value: raumFilter.find((d: InputDataType) => {
          const selectedYear = new Date(
            d.ZEIT.split('.').reverse().join('-'),
          ).getFullYear()
          return year === selectedYear && d.MERKMAL.includes('Benzin')
        }).WERT,
      },
      DieselAuto: {
        name: 'Diesel',
        value: raumFilter.find((d: InputDataType) => {
          const selectedYear = new Date(
            d.ZEIT.split('.').reverse().join('-'),
          ).getFullYear()
          return year === selectedYear && d.MERKMAL.includes('Diesel')
        }).WERT,
      },
      ElektroAuto: {
        name: 'Elektro',
        value: raumFilter.find((d: InputDataType) => {
          const selectedYear = new Date(
            d.ZEIT.split('.').reverse().join('-'),
          ).getFullYear()
          return year === selectedYear && d.MERKMAL.includes('Elektro')
        }).WERT,
      },
      HybridAuto: {
        name: 'Hybrid',
        value: raumFilter.find((d: InputDataType) => {
          const selectedYear = new Date(
            d.ZEIT.split('.').reverse().join('-'),
          ).getFullYear()
          return year === selectedYear && d.MERKMAL.includes('Hybrid')
        }).WERT,
      },
      SonstigesAuto: {
        name: 'Sonstige',
        value: raumFilter.find((d: InputDataType) => {
          const selectedYear = new Date(
            d.ZEIT.split('.').reverse().join('-'),
          ).getFullYear()
          return year === selectedYear && d.MERKMAL.includes('Sonstige')
        }).WERT,
      },
      // i wnat to get the total value for the autos for each year
      total: raumFilter
        .filter((d: InputDataType) => {
          const selectedYear = new Date(
            d.ZEIT.split('.').reverse().join('-'),
          ).getFullYear()
          return year === selectedYear
        })
        .reduce((acc: any, curr: { WERT: any }) => acc + curr.WERT, 0),
    }
  })

  const AllYears = {
    year: 'Alle Jahre',
    BenzinAuto: {
      name: 'Benzin',
      // i want the value with + sign if it is positive and - if it is negative
      value:
        outputData[outputData.length - 1].BenzinAuto.value -
        outputData[0].BenzinAuto.value,

      // value: outputData.reduce((acc, curr) => acc + curr.BenzinAuto.value, 0),
    },
    DieselAuto: {
      name: 'Diesel',
      value:
        outputData[outputData.length - 1].DieselAuto.value -
        outputData[0].DieselAuto.value,
    },
    ElektroAuto: {
      name: 'Elektro',
      value:
        outputData[outputData.length - 1].ElektroAuto.value -
        outputData[0].ElektroAuto.value,
    },

    HybridAuto: {
      name: 'Hybrid',
      value:
        outputData[outputData.length - 1].HybridAuto.value -
        outputData[0].HybridAuto.value,
    },
    SonstigesAuto: {
      name: 'Sonstige',
      value:
        outputData[outputData.length - 1].SonstigesAuto.value -
        outputData[0].SonstigesAuto.value,
    },
    total: outputData.reduce((acc, curr) => acc + curr.total, 0),
  }

  outputData.push(AllYears)

  return outputData
}

export default function useAutoData(timestamp: number | string) {
  const staticAutoData = dataPrepation()
  const [data] = useState<AutoDataType[]>(staticAutoData) // Use static data
  const [filteredData, setFilteredData] = useState<AutoDataType[]>()

  useEffect(() => {
    // For this example, just filter the data by year, based on the timestamp
    const filtered = data.filter(item => item.year === timestamp)
    setFilteredData(filtered)
  }, [timestamp, data])

  // Calculate the min and max values for the different types of autos
  const [totalMin, setTotalMin] = useState<number>(0)
  const [totalMax, setTotalMax] = useState<number>(0)

  useEffect(() => {
    if (!filteredData) {
      return
    }

    const min = Math.min(
      ...filteredData.flatMap(item => [
        item.BenzinAuto.value,
        item.DieselAuto.value,
        item.ElektroAuto.value,
        item.HybridAuto.value,
        item.SonstigesAuto.value,
      ]),
    )
    const max = Math.max(
      ...filteredData.flatMap(item => [
        item.BenzinAuto.value,
        item.DieselAuto.value,
        item.ElektroAuto.value,
        item.HybridAuto.value,
        item.SonstigesAuto.value,
      ]),
    )

    setTotalMin(min)
    setTotalMax(max)
  }, [filteredData])

  return {
    min: totalMin,
    max: totalMax,
    data: filteredData,
  }
}
