// export default useBarChartAutoData
import { useEffect, useState } from 'react'
import { dataPrepation } from './useAutoData'

type AutoDataType = {
  year: number | string
  BenzinAuto: {
    name: string
    value: number
  }
  DieselAuto: {
    name: string
    value: number
  }
  ElektroAuto: {
    name: string
    value: number
  }
  HybridAuto: {
    name: string
    value: number
  }
  SonstigesAuto: {
    name: string
    value: number
  }
}

type TransformedDataType = {
  title: string
  wert: {
    year: number | string
    value: number
  }[]
}

function mapBetween(
  currentNum: number,
  min: number,
  max: number,
  minAllowed = 0,
  maxAllowed = 90,
) {
  //i want return the integar value of this number

  return ((maxAllowed - minAllowed) * currentNum) / max
}

function transformData(): TransformedDataType[] {
  const data: AutoDataType[] = dataPrepation()

  const titles = [
    'BenzinAuto',
    'DieselAuto',
    'ElektroAuto',
    'HybridAuto',
    'SonstigesAuto',
  ] as const

  return titles.map(title => ({
    title: data[0][title].name,
    wert: data
      .filter(d => d.year !== 'Alle Jahre') // Exclude entries with 'Alle Jahre'
      .map(d => ({
        year: d.year,
        value: d[title].value,
      })),
  }))
}

function useBarChartAutoData(selectedTitle: string) {
  const [filteredData, setFilteredData] = useState<TransformedDataType | null>(
    null,
  )
  const [totalMin, setTotalMin] = useState<number>(0)
  const [totalMax, setTotalMax] = useState<number>(0)

  useEffect(() => {
    const staticAutoData = transformData()
    const filtered = staticAutoData.find(item => item.title === selectedTitle)
    setFilteredData(filtered || null)
  }, [selectedTitle])

  useEffect(() => {
    if (!filteredData) {
      return
    }

    const values = filteredData.wert.map(w => w.value)

    const min = Math.min(...values)
    const max = Math.max(...values)

    setTotalMin(min)
    setTotalMax(max)
  }, [filteredData])

  const ListofLabels: string[] = []
  const Listofprogress: number[] = []

  if (filteredData) {
    filteredData.wert.forEach(item => {
      ListofLabels.push(item.year.toString().slice(-2))
      Listofprogress.push(item.value)
      // Listofprogress.push(mapBetween(item.value, totalMin, totalMax))
    })
  }

  return {
    ListofLabels,
    Listofprogress,
  }
}

export default useBarChartAutoData
