import MobilityTile from '@/components/Tiles/Mobility/MobilityTile'
import { format } from 'date-fns'
import AutoChartContent from './AutoChartContent'

export default async function AutoChartTile() {
  return (
    <MobilityTile
      dataRetrieval={format(new Date(), '01.MM.yyyy')}
      dataSource="Kraftfahrt-Bundesamt"
      embedId="mobility-cars"
    >
      <AutoChartContent />
    </MobilityTile>
  )
}
