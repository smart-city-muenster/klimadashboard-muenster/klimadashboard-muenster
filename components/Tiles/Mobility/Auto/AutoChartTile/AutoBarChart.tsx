import { ReactECharts } from '@/components/Charts/ReactECharts'
import useDevice from '@/hooks/useDevice'
import { useContext } from 'react'
import { MaxChartValueContext } from './AutoChartContent'

type Props = {
  ListofLabels: string[]
  Listofprogress: number[]
  showLabels?: boolean
}

function BarChart({ ListofLabels, Listofprogress, showLabels = false }: Props) {
  const device = useDevice()
  const maxFromContext = useContext(MaxChartValueContext)

  return (
    <ReactECharts
      option={{
        grid: {
          left: 0,
          right: 0,
          top: 10,
          bottom: 0,
        },
        xAxis: [
          {
            type: 'category',

            axisTick: {
              show: false,
            },
          },
        ],
        yAxis: {
          type: 'value',
          show: true,
          max: maxFromContext,
          axisLabel: {
            show: false,
            inside: true,
            showMinLabel: false,
            fontSize: 12,
            backgroundColor: '#fff',
            formatter(value, index) {
              return Intl.NumberFormat('de-DE').format(value)
            },
          },
          axisTick: {
            show: false,
          },
          splitLine: {
            show: false,
            lineStyle: {
              type: 'dashed',
            },
          },
          interval: Math.ceil(maxFromContext / 10000) * 10000,
        },

        series: [
          {
            data: Listofprogress,
            type: 'bar',
            itemStyle: { color: 'rgb(52 193 123)' },
            barWidth: device === 'mobile' || device === 'tablet' ? 8 : 20,
            emphasis: {
              itemStyle: {
                color: 'rgb(52 193 123)',
              },
            },
          },
        ],
      }}
    ></ReactECharts>
  )
}

export default BarChart
