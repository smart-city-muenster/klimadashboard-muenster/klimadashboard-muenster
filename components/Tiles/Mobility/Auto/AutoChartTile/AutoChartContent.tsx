'use client'

import AnimatedNumber from '@/components/Elements/Animated/AnimatedNumber'
import Title from '@/components/Elements/Title'
import MobileSlider from '@/components/Inputs/MobileSlider'
import Slider from '@/components/Inputs/Slider'
import useAutoData from '@/hooks/useAutoData'
import useBarChartAutoData from '@/hooks/useBarChartAutoData'
import { subYears } from 'date-fns'
import { createContext, useEffect, useRef, useState } from 'react'
import { useWindowSize } from 'react-use'
import AutoCardHeader from './AutoCardHeader'
import AutoRow from './AutoRow'
import LoadingRow from './LoadingRow'

export const MaxChartValueContext = createContext(0)

export default function AutoChartContent() {
  const { width } = useWindowSize()
  const startDate = new Date()
  startDate.setFullYear(startDate.getFullYear() - 1)
  const lastYears: (number | string)[] = Array.from({ length: 7 }, (_, i) =>
    subYears(startDate, i).getFullYear(),
  ).reverse()

  lastYears.push('Alle Jahre')

  const [year, setYear] = useState<number | string>(
    lastYears[lastYears.length - 2],
  )

  const { data, min, max } = useAutoData(year)
  const isBar = year === 'Alle Jahre'
  const loading = !data

  const [contentHeight, setContentHeight] = useState('auto')
  const contentRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (contentRef.current) {
      setContentHeight(`${contentRef.current.scrollHeight + 40}px `)
    }
  }, [year, data])

  const { ListofLabels: BenzinLabels, Listofprogress: BenzinProgress } =
    useBarChartAutoData('Benzin')
  const { ListofLabels: DieselLabels, Listofprogress: DieselProgress } =
    useBarChartAutoData('Diesel')
  const { ListofLabels: ElektroLabels, Listofprogress: ElektroProgress } =
    useBarChartAutoData('Elektro')
  const { ListofLabels: HybridLabels, Listofprogress: HybridProgress } =
    useBarChartAutoData('Hybrid')
  const { ListofLabels: SonstigesLabels, Listofprogress: SonstigesProgress } =
    useBarChartAutoData('Sonstige')
  const total =
    data?.reduce((acc, cur) => {
      return (
        acc +
        cur.BenzinAuto.value +
        cur.DieselAuto.value +
        cur.ElektroAuto.value +
        cur.HybridAuto.value +
        cur.SonstigesAuto.value
      )
    }, 0) ?? 0

  const maxOfAll = Math.max(
    ...[
      ...BenzinProgress,
      ...DieselProgress,
      ...ElektroProgress,
      ...HybridProgress,
      ...SonstigesProgress,
    ],
  )
  return (
    <>
      <AutoCardHeader
        AllYear={lastYears[0]}
        title={
          <span>
            {isBar && total > 0 && '+'} <AnimatedNumber>{total}</AnimatedNumber>
          </span>
        }
        total={total}
        year={year}
      />
      <div
        className="overflow-hidden rounded bg-white px-4 py-2 transition-all duration-[1000ms]"
        style={{ height: `calc(${contentHeight})` }}
      >
        <div ref={contentRef}>
          {loading &&
            new Array(5).fill(undefined).map((_, i) => <LoadingRow key={i} />)}
          {data &&
            data.length > 0 &&
            data.map((e, index) => (
              <div key={index}>
                {isBar && (
                  <div className="my-4 flex w-full items-end pr-4 lg:gap-12">
                    <div className="w-28 flex-none md:w-40"></div>
                    <div className="h-full flex-1 md:mb-0">
                      <div className="flex h-full items-end md:items-center lg:items-end">
                        <div className="hidden w-full justify-around xs:flex md:hidden lg:flex">
                          {lastYears.slice(0, -1).map((year, index) => {
                            return (
                              <Title
                                className="sm:text-xs"
                                key={index}
                                variant={'primary'}
                              >
                                {year.toString().slice(2, 4)}
                              </Title>
                            )
                          })}
                        </div>
                        <div className="flex w-full justify-between text-center xs:hidden md:flex lg:hidden">
                          <Title
                            className="sm:text-xs"
                            key={index}
                            variant={'primary'}
                          >
                            {lastYears.at(0)}
                          </Title>
                          <Title
                            className="sm:text-xs"
                            key={index}
                            variant={'primary'}
                          >
                            -
                          </Title>
                          <Title
                            className="sm:text-xs"
                            key={index}
                            variant={'primary'}
                          >
                            {lastYears.at(-2)}
                          </Title>
                        </div>
                        <div className="min-w-[0px] md:min-w-[0px] lg:min-w-[120px] between-1440-and-1600:min-w-[0px]" />
                      </div>
                    </div>
                  </div>
                )}
                <MaxChartValueContext.Provider value={maxOfAll}>
                  <AutoRow
                    bar={isBar}
                    count={e.BenzinAuto.value}
                    ListofLabels={BenzinLabels}
                    Listofprogress={BenzinProgress}
                    max={max}
                    min={min}
                    name={e.BenzinAuto.name}
                    showLabels={true}
                  />
                  <AutoRow
                    bar={isBar}
                    count={e.DieselAuto.value}
                    ListofLabels={DieselLabels}
                    Listofprogress={DieselProgress}
                    max={max}
                    min={min}
                    name={e.DieselAuto.name}
                  />
                  <AutoRow
                    bar={isBar}
                    count={e.ElektroAuto.value}
                    ListofLabels={ElektroLabels}
                    Listofprogress={ElektroProgress}
                    max={max}
                    min={min}
                    name={e.ElektroAuto.name}
                  />
                  <AutoRow
                    bar={isBar}
                    count={e.HybridAuto.value}
                    ListofLabels={HybridLabels}
                    Listofprogress={HybridProgress}
                    max={max}
                    min={min}
                    name={e.HybridAuto.name}
                  />
                  <AutoRow
                    bar={isBar}
                    count={e.SonstigesAuto.value}
                    ListofLabels={SonstigesLabels}
                    Listofprogress={SonstigesProgress}
                    max={max}
                    min={min}
                    name={e.SonstigesAuto.name}
                  />
                </MaxChartValueContext.Provider>
              </div>
            ))}
        </div>
      </div>

      {lastYears.length > 0 && width > 900 && (
        <Slider
          defaultValue={[lastYears.length - 2]}
          firstValueMobile={lastYears.length - 1}
          labels={lastYears.map(year => year.toString())}
          // make value 44 to make the pointer in the middle
          labelWidth={58}
          max={lastYears.length - 1}
          min={0}
          onValueChange={([e]) => {
            setYear(lastYears[e])
          }}
          variant="mobility"
        />
      )}
      {lastYears.length > 0 && width <= 900 && (
        <MobileSlider
          defaultValue={[lastYears.length - 2]}
          firstValueMobile={lastYears.length - 2}
          labels={lastYears.map(year => year.toString())}
          max={lastYears.length - 1}
          min={0}
          onValueChange={([e]) => {
            setYear(lastYears[e])
          }}
          variant="mobility"
        />
      )}
    </>
  )
}
