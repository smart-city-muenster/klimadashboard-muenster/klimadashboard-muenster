import * as ProgressPrimitive from '@radix-ui/react-progress'

export default function AutoProgress({ progress }: { progress: number }) {
  const myProgress = progress > 0 ? progress : 0
  // const progressWidth = {myProgress}
  // // const progressWidth = 15
  // const minWidth = 10

  return (
    <div className="flex w-full">
      <ProgressPrimitive.Root
        // className="w-[calc(100%-6rem)]"
        className="w-full"
        value={myProgress}
      >
        <ProgressPrimitive.Indicator
          className="relative mt-[0.8px] flex h-1.5 bg-mobility transition-all md:mt-[4.5px] md:h-3"
          // style={{ width: `max(${progressWidth}, ${minWidth}rem)` }}
          style={{ width: `${myProgress}%` }}
        ></ProgressPrimitive.Indicator>
      </ProgressPrimitive.Root>
    </div>
  )
}
