import { Spacer } from '@/components/Elements/Spacer'
import Title from '@/components/Elements/Title'
// @ts-ignore
import AnimatedNumber from '@/components/Elements/Animated/AnimatedNumber'
// @ts-ignore
import Einwohner from '@/assets/data/einwohner.csv'

interface InputDataType {
  Zeit: string
  BEVSTD__Bevoelkerungsstand__Anzahl: number
}
interface CsvRow {
  Statistik_Code: string
  Statistik_Label: string
  Zeit_Code: string
  Zeit_Label: string
  Zeit: string
  '1_Merkmal_Code': string
  '1_Merkmal_Label': string
  '1_Auspraegung_Code': string
  '1_Auspraegung_Label': string
  '2_Merkmal_Code': string
  '2_Merkmal_Label': string
  '2_Auspraegung_Code': string
  '2_Auspraegung_Label': string
  BEVSTD__Bevoelkerungsstand__Anzahl: number
}

function filterEinwoherCols() {
  const filteredData = Einwohner.filter(
    (row: CsvRow) => row['2_Auspraegung_Label'] === 'Insgesamt',
  ).map((row: CsvRow) => ({
    Zeit: row.Zeit.split('.')[2],
    BEVSTD__Bevoelkerungsstand__Anzahl: row.BEVSTD__Bevoelkerungsstand__Anzahl,
  }))

  return filteredData
}

function AutoCardHeader(props: {
  title: React.ReactNode
  year: string | number
  AllYear: string | number
  total: number
}) {
  const einwohnerPerSelectedYear = filterEinwoherCols().filter(
    (d: InputDataType) => d.Zeit === String(props.year),
  )

  const pkwProEinwohner =
    einwohnerPerSelectedYear.length > 0
      ? parseFloat(
          (
            props.total /
            einwohnerPerSelectedYear[0].BEVSTD__Bevoelkerungsstand__Anzahl
          ).toFixed(3),
        )
      : 0

  return (
    <>
      <div className="flex flex-wrap justify-start gap-x-4 lg:max-w-[87%]">
        <Title
          as={'h1'}
          className="min-w-fit tracking-tighter xl:text-6xl 2xl:text-7xl"
          font={'normal'}
          variant={'mobility'}
        >
          {props.title} Autos
        </Title>

        <Title as={'subtitle'} color="dark">
          {props.year !== 'Alle Jahre' ? (
            <>
              waren <span className="text-mobility">{props.year}</span> in
              Münster angemeldet.{' '}
              {pkwProEinwohner > 0 ? (
                <>
                  Das waren{' '}
                  <span className="text-mobility">
                    <AnimatedNumber
                      className="inline-block w-14 lg:w-20 "
                      decimals={3}
                    >
                      {pkwProEinwohner}
                    </AnimatedNumber>{' '}
                    PKW
                  </span>{' '}
                  pro Einwohner*in.{' '}
                </>
              ) : (
                <br />
              )}
              Aufgeteilt auf folgende Antriebsarten:
            </>
          ) : (
            <>
              sind <span className="text-mobility">seit {props.AllYear}</span>{' '}
              in Münster hinzugekommen, aufgeteilt auf folgende Antriebsarten:
            </>
          )}
        </Title>
      </div>
      <Spacer />
    </>
  )
}

export default AutoCardHeader
