import AnimatedNumber from '@/components/Elements/Animated/AnimatedNumber'
import Title from '@/components/Elements/Title'
import {
  MsKlimadashboardIconsMPKwBenzin,
  MsKlimadashboardIconsMPKwDiesel,
  MsKlimadashboardIconsMPKwElektro,
  MsKlimadashboardIconsMPKwHybrid,
  MsKlimadashboardIconsMPKwSontige,
} from '@/components/Icons/Mobilitaet'
import useDevice from '@/hooks/useDevice'
import { useRef } from 'react'
import BarChart from './AutoBarChart'
import AutoProgress from './AutoProgress'

type AutoRowProps = {
  name: string
  count: number
  min: number
  max: number
  bar: boolean
  ListofLabels: string[]
  Listofprogress: number[]
  showLabels?: boolean
}

function mapBetween(
  currentNum: number,
  min: number,
  max: number,
  minAllowed = 0,
  maxAllowed = 100,
) {
  return (
    ((maxAllowed - minAllowed) * (currentNum - min)) / (max - min) + minAllowed
  )
}

// prettier-ignore
const iconMapping: Record<string, (props: React.SVGProps<SVGSVGElement>) => React.JSX.Element> = {
  Benzin: MsKlimadashboardIconsMPKwBenzin,
  Diesel: MsKlimadashboardIconsMPKwDiesel,
  Elektro: MsKlimadashboardIconsMPKwElektro,
  Hybrid: MsKlimadashboardIconsMPKwHybrid,
  Sonstige: MsKlimadashboardIconsMPKwSontige,
}

export default function AutoRow({
  name,
  count,
  min,
  max,
  bar,
  ListofLabels,
  Listofprogress,
  showLabels,
}: AutoRowProps) {
  const ref = useRef<HTMLDivElement>(null)
  const progressBarParentWidth = ref.current?.offsetWidth ?? 1
  const minBarWidthTablet = (80 / progressBarParentWidth) * 100
  const minBarWidthDesktop = (120 / progressBarParentWidth) * 100
  const device = useDevice()
  const progress =
    device === 'desktop'
      ? mapBetween(count, min * 0.9, max * 1.1, 0)
      : device === 'tablet'
      ? mapBetween(count, min * 0.9, max * 1.1, 0)
      : mapBetween(count, min * 0.9, max * 1.1, 0)

  const Icon = iconMapping[name]

  if (bar) {
    return (
      <div className="my-4 flex h-[72px] w-full items-end pr-4 lg:gap-12">
        <div className="w-28 flex-none md:w-40">
          <Title as="h5" variant="primary">
            {name}
          </Title>
          <Title as="h3" variant="mobility">
            {bar && count > 0 ? '+' : ''}
            <AnimatedNumber>{count}</AnimatedNumber>
          </Title>
        </div>
        <div className="flex h-full flex-1 items-end border-b-2 border-mobility md:mb-0">
          <BarChart
            ListofLabels={ListofLabels}
            Listofprogress={Listofprogress}
            showLabels={showLabels}
          />

          <Icon className="hidden h-8 min-w-fit lg:block lg:h-12 between-1440-and-1600:hidden" />
        </div>
      </div>
    )
  }

  return (
    <div className="my-4 flex w-full items-end lg:gap-12">
      <div className="w-28 flex-none md:w-40">
        <Title as="h5" variant="primary">
          {name}
        </Title>
        <Title as="h3" variant="mobility">
          {bar && count > 0 ? '+' : ''}
          <AnimatedNumber>{count}</AnimatedNumber>
        </Title>
      </div>
      <div className="relative w-full flex-1 md:mb-0" ref={ref}>
        <Icon
          className="absolute bottom-[6px] mb-1 h-8 transition-all lg:h-12"
          style={{
            right: progress < 15 ? 'unset' : `calc(100% - ${progress}%)`,
          }}
        />
        <AutoProgress progress={progress} />
      </div>
    </div>
  )
}
