import * as React from 'react'
import type { SVGProps } from 'react'
function SvgMsKlimadashboardIconsMPkwElektro(props: SVGProps<SVGSVGElement>) {
  return (
    <svg
      data-name="Ebene 1"
      id="MS_Klimadashboard_Icons_m_Pkw_Elektro_svg__Ebene_1"
      viewBox="0 0 120 48"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <defs>
        <style>
          {
            '.MS_Klimadashboard_Icons_m_Pkw_Elektro_svg__cls-1{fill:none}.MS_Klimadashboard_Icons_m_Pkw_Elektro_svg__cls-2{fill:#34c17b}'
          }
        </style>
      </defs>
      <g
        data-name="Gruppe 1329"
        id="MS_Klimadashboard_Icons_m_Pkw_Elektro_svg__Gruppe_1329"
      >
        <path
          className="MS_Klimadashboard_Icons_m_Pkw_Elektro_svg__cls-1"
          d="M-8.15-13.09h66.61v66.61H-8.15z"
          data-name="Rechteck 288"
          id="MS_Klimadashboard_Icons_m_Pkw_Elektro_svg__Rechteck_288"
        />
      </g>
      <g
        data-name="Gruppe 1369"
        id="MS_Klimadashboard_Icons_m_Pkw_Elektro_svg__Gruppe_1369"
      >
        <path
          className="MS_Klimadashboard_Icons_m_Pkw_Elektro_svg__cls-2"
          d="M71.28 40.37v.14c0 .04 0 .07-.02.11 0-.09 0-.17.03-.25Z"
        />
        <path
          className="MS_Klimadashboard_Icons_m_Pkw_Elektro_svg__cls-2"
          d="M114.06 23.62h-8.63l-1.03-6.05c-2.26-8.09-7.47-9.13-11.75-9.13H71.68c-3.85 0-12.67 1.51-13.12 15.27a8.601 8.601 0 0 0-6.85 8.7v4.29a4.933 4.933 0 0 0 4.74 5.13h1.69a7.867 7.867 0 0 0 7.65 5.96c3.66 0 6.78-2.52 7.65-5.97h21.48a7.86 7.86 0 0 0 7.66 5.97c3.66 0 6.8-2.51 7.65-5.97h5c2.65-.12 4.72-2.36 4.62-5v-6.89a6.073 6.073 0 0 0-5.79-6.32ZM71.28 40.37v.14c0 .04 0 .07-.02.11v.02a5.512 5.512 0 0 1-4.87 4.75 5.507 5.507 0 0 1-6.07-4.89c-.03-.2-.04-.39-.03-.59 0-3.05 2.47-5.52 5.51-5.53 3.05 0 5.52 2.49 5.52 5.53 0 .17 0 .33-.02.47Zm36.77.14a5.499 5.499 0 0 1-4.87 4.87c-2.99.34-5.67-1.8-6.07-4.75 0-.04-.02-.09-.02-.13v-.14c0-.16-.02-.32-.02-.47 0-3.05 2.46-5.51 5.51-5.53 3.05.02 5.51 2.48 5.52 5.53 0 .22 0 .4-.03.61Zm9.42-3.68c.1 1.34-.91 2.5-2.24 2.61h-4.76c-.19-4.24-3.71-7.61-7.98-7.56-4.22.05-7.64 3.39-7.81 7.56H73.67c-.19-4.23-3.7-7.6-7.97-7.55a7.884 7.884 0 0 0-7.8 7.56h-1.45a2.561 2.561 0 0 1-2.34-2.73v-4.3c0-3.54 2.54-6.41 5.63-6.41.66 0 1.19-.53 1.19-1.19 0-11.56 5.85-13.98 10.75-13.98h20.96c3.84 0 7.65.9 9.43 7.24l1.15 6.94c.1.58.59 1.01 1.18 1h9.65c2.02.17 3.54 1.91 3.4 3.94v6.89Z"
        />
      </g>
      <g
        data-name="Gruppe 1371"
        id="MS_Klimadashboard_Icons_m_Pkw_Elektro_svg__Gruppe_1371"
      >
        <path
          className="MS_Klimadashboard_Icons_m_Pkw_Elektro_svg__cls-2"
          d="M48.91 23.72c-.07.33-.27.6-.56.78-.2.12-.42.18-.65.18-.4 0-.79-.19-1.03-.54l-3.06-4.45.02 1.01c-.06.77-.68 1.36-1.42 1.36l-13.86.08c-.49 0-.94-.28-1.18-.72-.25-.48-.21-1.04.09-1.43l4.87-7.73-28.11.05c-.66 0-1.18-.5-1.2-1.15-.01-.3.11-.59.32-.82.22-.23.51-.36.83-.37l29.84-.07c.53-.05.95.14 1.25.55.32.49.32 1.14 0 1.59l-4.92 7.75 11.03-.05-.49-4.04v-.21c.03-.56.38-1.05.89-1.26.47-.2 1-.12 1.4.22.09.08.16.18.23.27l5.52 8.07c.19.27.26.61.19.93Z"
        />
        <path
          className="MS_Klimadashboard_Icons_m_Pkw_Elektro_svg__cls-2"
          d="m42.26 28.85-.04.11c-.37 1.06-.71 2.05-1.25 3.73-.07.2-.12.35-.15.44l-.02.08c-.15.5-.6.84-1.13.84h-.29l.04-.04s-.03-.01-.05-.01c-.3-.08-.55-.28-.7-.55-.15-.27-.19-.58-.11-.88l.02-.08c.03-.09.09-.28.17-.54.42-1.29.6-1.86.8-2.53l-23.16.49c-.42 0-.83-.23-1.03-.6-.23-.38-.21-.85.04-1.21l4.93-7.42-18.72.06c-.28.02-.56-.07-.79-.28-.24-.21-.39-.53-.4-.85-.02-.65.48-1.19 1.12-1.21l20.98-.11c.41-.04.82.19 1.05.59.22.38.2.85-.05 1.22l-4.93 7.43 22.38-.46h.01c.44 0 .84.2 1.1.55.26.36.32.81.18 1.23Z"
        />
      </g>
      <path
        className="MS_Klimadashboard_Icons_m_Pkw_Elektro_svg__cls-1"
        d="M0 0h120v48H0z"
      />
    </svg>
  )
}
export default SvgMsKlimadashboardIconsMPkwElektro
