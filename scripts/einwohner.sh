#!/bin/bash

# This script is used to download the CSV file from the provided URL and move it to the assets/data folder

# Download the CSV file
curl -o einwohner.csv "https://www.regionalstatistik.de/genesisws/rest/2020/data/tablefile?username=GAST&password=GAST&name=12411-01-01-4&area=free&compress=false&transpose=false&startyear=%20&endyear=%20&timeslices=7&regionalkey=05515&format=ffcsv&job=false&stand=01.01.1970%2001%3A00&language=de"

# Move the CSV file to the assets/data folder (relative path from the root of the project)
mv einwohner.csv assets/data/einwohner.csv

# Exit the script
exit 0
