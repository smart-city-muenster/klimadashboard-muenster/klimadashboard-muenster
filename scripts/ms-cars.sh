#!/bin/bash

# This script is used to download the traffic load data from gitlab and move it to the assets/data folder

# Download the traffic load data from the server
curl -o ms_fahrzeuge.csv "https://www.stadt-muenster.de/fileadmin/user_upload/stadt-muenster/61_stadtentwicklung/pdf/sms/05515000_csv_fahrzeuge.csv"

# Move the traffic load data to the assets/data folder (relative path from the root of the project)
mv ms_fahrzeuge.csv assets/data/ms_fahrzeuge.csv

# Exit the script
exit 0